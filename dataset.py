import requests
import json

# Define API endpoints
dnd5eapi_endpoint = "https://www.dnd5eapi.co/api"
open5eapi_endpoint = "https://api.open5e.com"

# Define content types and their corresponding endpoints for D&D 5th Edition API
dnd5eapi_content_types = {
    "NPCs": "/monsters",
    "Quests": "/quests",
    "Items": "/items",
    "Spells": "/spells",
    "Races": "/races",
    "Classes": "/classes",
    "Dialogue": "/dialogues",  # Assuming an endpoint for dialogue data
    "Stories": "/stories"     # Assuming an endpoint for stories data
}

# Define content types and their corresponding endpoints for Open5e API
open5eapi_content_types = {
    "Monsters": "/monsters",
    "Spells": "/spells",
    "Equipment": "/equipment",
    "Conditions": "/conditions",
    "Weapons": "/weapons",
    "Dialogue": "/dialogues",  # Assuming an endpoint for dialogue data
    "Stories": "/stories"     # Assuming an endpoint for stories data
}

# Initialize an empty dictionary to store the generated content
generated_content = {}

# Fetch data from D&D 5th Edition API
for content_type, endpoint in dnd5eapi_content_types.items():
    response = requests.get(f"{dnd5eapi_endpoint}{endpoint}")
    
    if response.status_code == 200:
        data = response.json()
        results = data.get("results", [])
        
        if results:
            generated_content[f"D&D 5E - {content_type}"] = [result["name"] for result in results]
    else:
        print(f"Failed to fetch data for {content_type} from D&D 5E API")

# Fetch data from Open5e API
for content_type, endpoint in open5eapi_content_types.items():
    response = requests.get(f"{open5eapi_endpoint}{endpoint}")
    
    if response.status_code == 200:
        data = response.json()
        results = data.get("results", [])
        
        if results:
            generated_content[f"Open5e - {content_type}"] = [result["name"] for result in results]
    else:
        print(f"Failed to fetch data for {content_type} from Open5e API")

# Save the generated content to a JSON file
with open('dnd_content.json', 'w') as json_file:
    json.dump(generated_content, json_file)

# Print generated content examples
for content_type, examples in generated_content.items():
    print(f"{content_type}:")
    for i, example in enumerate(examples, start=1):
        print(f"{i}. {example}")
    print("\n")
