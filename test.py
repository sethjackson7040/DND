import pygame
import sys

# Initialize Pygame
pygame.init()

# Constants
WIDTH, HEIGHT = 800, 600
FPS = 60

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (169, 169, 169)  # Gray color for the highlight effect

# Character Sets
classes = [
    "Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Paladin",
    "Ranger", "Rogue", "Sorcerer", "Warlock", "Wizard", "Artificer", "Blood Hunter"
]

races = [
    "Dragonborn", "Dwarf", "Elf", "Gnome", "Half-Elf", "Halfling", "Half-Orc", "Human", "Tiefling"
]

alignments = [
    "Lawful Good", "Neutral Good", "Chaotic Good",
    "Lawful Neutral", "True Neutral", "Chaotic Neutral",
    "Lawful Evil", "Neutral Evil", "Chaotic Evil"
]

# Stat Allocation Constants
initial_stat_value = 8  # Initial value for all stats
total_points = 27  # Total points for allocation
max_stat_value = 15  # Maximum value for a single stat

# Create the game window
screen = pygame.display.set_mode((WIDTH, HEIGHT), pygame.NOFRAME | pygame.FULLSCREEN)
pygame.display.set_caption("D&D Text Adventure")

# Function to draw text on the screen
def draw_text(text, font, color, surface, x, y):
    text_obj = font.render(text, True, color)
    text_rect = text_obj.get_rect()
    text_rect.topleft = (x, y)
    surface.blit(text_obj, text_rect)

# Function to get character name input from the user
def get_character_name(screen, font):
    input_box = pygame.Rect(250, 340, 140, 32)
    color_inactive = pygame.Color('lightskyblue3')
    color_active = pygame.Color('dodgerblue2')
    color = color_inactive
    active = False
    text = ''
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if input_box.collidepoint(event.pos):
                    active = not active
                else:
                    active = False
                color = color_active if active else color_inactive
            if event.type == pygame.KEYDOWN:
                if active:
                    if event.key == pygame.K_RETURN:
                        if text:
                            return text  # Exit the loop when Enter is pressed and text is not empty
                    elif event.key == pygame.K_BACKSPACE:
                        text = text[:-1]
                    else:
                        text += event.unicode
        screen.fill(WHITE)
        txt_surface = font.render(text, True, color)
        width = max(200, txt_surface.get_width() + 10)
        input_box.w = width
        screen.blit(txt_surface, (input_box.x + 5, input_box.y + 5))
        pygame.draw.rect(screen, color, input_box, 2)
        pygame.display.flip()

def choose_alignment(screen, font):
    alignment_text = font.render("Select your character's alignment:", True, BLACK)
    
    alignment_options = alignments

    option_rects = []
    for i, option in enumerate(alignment_options):
        text = font.render(option, True, BLACK)
        text_rect = text.get_rect(topleft=(50, 100 + i * 40))
        option_rects.append(text_rect)

    selected_alignment = None

    selecting_alignment = True
    while selecting_alignment:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                for i, rect in enumerate(option_rects):
                    if rect.collidepoint(event.pos):
                        option_rects[i] = pygame.draw.rect(screen, GRAY, rect)
                    else:
                        option_rects[i] = pygame.draw.rect(screen, WHITE, rect)
            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, rect in enumerate(option_rects):
                    if rect.collidepoint(event.pos):
                        selected_alignment = alignment_options[i]
                        selecting_alignment = False

        screen.fill(WHITE)
        screen.blit(alignment_text, (50, 50))
        for i, option in enumerate(alignment_options):
            screen.blit(font.render(option, True, BLACK), (50, 100 + i * 40))
        
        pygame.display.flip()

    return selected_alignment

# Function for character backstory customization
def customize_backstory(screen, font):
    font = pygame.font.Font(None, 20)  # Adjust the font size (e.g., 24)
    backstory_text = font.render("Select your character's backstory:", True, BLACK)
    
    backstory_options = [
        "You are an orphan who grew up on the streets, surviving through cunning and thievery.",
        "You were once a respected scholar until you stumbled upon forbidden knowledge that changed your life forever.",
        "You are the heir to a noble family, burdened by the responsibilities and expectations of your lineage.",
        "You lived a quiet life as a farmer until a traumatic event forced you into a life of adventure.",
        "You were a soldier in a great war, scarred by the horrors of battle and seeking redemption.",
    ]

    option_rects = []
    for i, option in enumerate(backstory_options):
        text = font.render(option, True, BLACK)
        text_rect = text.get_rect(topleft=(50, 100 + i * 40))
        option_rects.append(text_rect)

    selected_backstory = None

    selecting_backstory = True
    while selecting_backstory:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                for i, rect in enumerate(option_rects):
                    if rect.collidepoint(event.pos):
                        option_rects[i] = pygame.draw.rect(screen, GRAY, rect)
                    else:
                        option_rects[i] = pygame.draw.rect(screen, WHITE, rect)
            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, rect in enumerate(option_rects):
                    if rect.collidepoint(event.pos):
                        selected_backstory = backstory_options[i]
                        selecting_backstory = False

        screen.fill(WHITE)
        screen.blit(backstory_text, (50, 50))
        for i, option in enumerate(backstory_options):
            screen.blit(font.render(option, True, BLACK), (50, 100 + i * 40))
        
        pygame.display.flip()

    return selected_backstory

# Function to allocate stats to the character
def allocate_stats(screen, font):
    stats = {
        "Strength": initial_stat_value,
        "Dexterity": initial_stat_value,
        "Constitution": initial_stat_value,
        "Intelligence": initial_stat_value,
        "Wisdom": initial_stat_value,
        "Charisma": initial_stat_value
    }

    points_remaining = total_points

    font = pygame.font.Font(None, 30)
    button_font = pygame.font.Font(None, 24)

    stat_texts = [font.render(f"{stat}: {value}", True, BLACK) for stat, value in stats.items()]
    stat_rects = [text.get_rect(topleft=(50, 150 + i * 40)) for i, text in enumerate(stat_texts)]

    # Define button positions and sizes
    button_width, button_height = 30, 30
    plus_buttons = [pygame.Rect(220, 150 + i * 40, button_width, button_height) for i in range(len(stats))]
    minus_buttons = [pygame.Rect(260, 150 + i * 40, button_width, button_height) for i in range(len(stats))]
    confirm_button = pygame.Rect(50, 500, 150, 40)  # Adjusted the Y position of the confirm button

    selected_stat_index = 0

    character_name = ""
    name_input_box = pygame.Rect(300, 280, 140, 32)

    allocating_stats = True
    while allocating_stats:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                for i, rect in enumerate(stat_rects):
                    if rect.collidepoint(event.pos):
                        stat_texts[i] = font.render(f"{list(stats.keys())[i]}: {list(stats.values())[i]}", True, GRAY)
                    else:
                        stat_texts[i] = font.render(f"{list(stats.keys())[i]}: {list(stats.values())[i]}", True, BLACK)

            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, plus_button in enumerate(plus_buttons):
                    if plus_button.collidepoint(event.pos):
                        stat_name = list(stats.keys())[i]
                        if points_remaining > 0 and stats[stat_name] < max_stat_value:
                            stats[stat_name] += 1
                            points_remaining -= 1
                    elif minus_buttons[i].collidepoint(event.pos):
                        stat_name = list(stats.keys())[i]
                        if stats[stat_name] > initial_stat_value:
                            stats[stat_name] -= 1
                            points_remaining += 1

                if confirm_button.collidepoint(event.pos) and points_remaining == 0 and character_name:
                    allocating_stats = False  # Exit the stat allocation loop

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_TAB:
                    pass  # Optionally handle tab key
                elif event.key == pygame.K_RETURN:
                    pass  # Optionally handle Enter key
                elif event.key == pygame.K_BACKSPACE:
                    character_name = character_name[:-1]
                else:
                    character_name += event.unicode

        screen.fill(WHITE)

        remaining_text = font.render(f"Points Remaining: {points_remaining}", True, BLACK)

        # Draw stat texts with names and values
        for i, text_surface in enumerate(stat_texts):
            # Get the rect for the text surface
            text_rect = text_surface.get_rect(topleft=(50, 150 + i * 40))
            
            # Remove the box around the "Strength" stat
            if i == selected_stat_index:
                pygame.draw.rect(screen, WHITE, text_rect, 0)
            
            screen.blit(text_surface, text_rect)

        # Draw plus and minus buttons
        for i, button in enumerate(plus_buttons):
            pygame.draw.rect(screen, BLACK, button, 1)
            plus_text = button_font.render("+", True, BLACK)
            screen.blit(plus_text, (button.left + 10, button.top + 3))

        for i, button in enumerate(minus_buttons):
            pygame.draw.rect(screen, BLACK, button, 1)
            minus_text = button_font.render("-", True, BLACK)
            screen.blit(minus_text, (button.left + 10, button.top + 3))

        # Draw confirm button
        pygame.draw.rect(screen, BLACK, confirm_button, 2)
        confirm_text = font.render("Confirm", True, BLACK)
        screen.blit(confirm_text, (confirm_button.centerx - confirm_text.get_width() // 2, confirm_button.centery - confirm_text.get_height() // 2))

        # Draw character name input box
        pygame.draw.rect(screen, BLACK, name_input_box, 2)
        txt_surface = font.render(character_name, True, BLACK)
        width = max(200, txt_surface.get_width() + 10)
        name_input_box.w = width
        screen.blit(txt_surface, (name_input_box.x + 5, name_input_box.y + 5))

        # Draw remaining points text
        screen.blit(remaining_text, (50, 400))

        pygame.display.flip()

    return stats, character_name

# Function for character backstory customization
def choose_character():
    selected_class = None
    selected_race = None
    selected_alignment = None  # Added alignment selection
    character_name = ""

    font = pygame.font.Font(None, 36)
    class_text = font.render("Choose a character class:", True, BLACK)
    race_text = font.render("Choose a character race:", True, BLACK)
    alignment_text = font.render("Choose a character alignment:", True, BLACK)  # Added alignment text

    class_rect = class_text.get_rect(topleft=(20, 20))
    race_rect = race_text.get_rect(topleft=(20, 80))
    alignment_rect = alignment_text.get_rect(topleft=(20, 140))  # Position alignment text

    class_options = [font.render(c, True, BLACK) for c in classes]
    race_options = [font.render(r, True, BLACK) for r in races]
    alignment_options = [font.render(a, True, BLACK) for a in alignments]  # Added alignment options

    class_rects = [option.get_rect(topleft=(40, 80 + i * 40)) for i, option in enumerate(class_options)]
    race_rects = [option.get_rect(topleft=(40, 140 + i * 40)) for i, option in enumerate(race_options)]
    alignment_rects = [option.get_rect(topleft=(40, 200 + i * 40)) for i, option in enumerate(alignment_options)]  # Position alignment options

    selected_class_option = None
    selected_race_option = None
    selected_alignment_option = None  # Added selected_alignment_option

    choosing_class = True
    while choosing_class:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                for i, rect in enumerate(class_rects):
                    if rect.collidepoint(event.pos):
                        class_options[i] = font.render(classes[i], True, GRAY)
                    else:
                        class_options[i] = font.render(classes[i], True, BLACK)

            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, rect in enumerate(class_rects):
                    if rect.collidepoint(event.pos):
                        selected_class = classes[i]
                        selected_class_option = i
                        choosing_class = False

        screen.fill(WHITE)
        screen.blit(class_text, class_rect)
        for i, opt in enumerate(class_options):
            screen.blit(opt, class_rects[i])

        pygame.display.flip()

    choosing_race = True
    while choosing_race:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                for i, rect in enumerate(race_rects):
                    if rect.collidepoint(event.pos):
                        race_options[i] = font.render(races[i], True, GRAY)
                    else:
                        race_options[i] = font.render(races[i], True, BLACK)

            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, rect in enumerate(race_rects):
                    if rect.collidepoint(event.pos):
                        selected_race = races[i]
                        selected_race_option = i
                        choosing_race = False

        screen.fill(WHITE)
        screen.blit(race_text, race_rect)
        for i, opt in enumerate(race_options):
            screen.blit(opt, race_rects[i])

        pygame.display.flip()

    choosing_alignment = True
    while choosing_alignment:  # Alignment selection loop
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                for i, rect in enumerate(alignment_rects):
                    if rect.collidepoint(event.pos):
                        alignment_options[i] = font.render(alignments[i], True, GRAY)
                    else:
                        alignment_options[i] = font.render(alignments[i], True, BLACK)

            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, rect in enumerate(alignment_rects):
                    if rect.collidepoint(event.pos):
                        selected_alignment = alignments[i]
                        selected_alignment_option = i
                        choosing_alignment = False

        screen.fill(WHITE)
        screen.blit(alignment_text, alignment_rect)  # Display alignment text
        for i, opt in enumerate(alignment_options):
            screen.blit(opt, alignment_rects[i])  # Display alignment options

        pygame.display.flip()

    if selected_race is not None:
        # Allocate stats
        stats, character_name = allocate_stats(screen, font)

    # Get character backstory
    character_backstory = customize_backstory(screen, font)
    return selected_class, selected_race, selected_alignment, character_name, stats  # Include selected_alignment


# Main menu function
def main_menu():
    menu_font = pygame.font.Font(None, 36)

    new_game_text = "New Game"
    load_game_text = "Load Game"
    new_game_color = BLACK
    load_game_color = BLACK

    new_game_rect = menu_font.render(new_game_text, True, new_game_color).get_rect()
    new_game_rect.center = (WIDTH // 2, HEIGHT // 3)

    load_game_rect = menu_font.render(load_game_text, True, load_game_color).get_rect()
    load_game_rect.center = (WIDTH // 2, HEIGHT // 2)

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEMOTION:
                if new_game_rect.collidepoint(event.pos):
                    new_game_color = GRAY  # Highlighted color
                else:
                    new_game_color = BLACK  # Default color

                if load_game_rect.collidepoint(event.pos):
                    load_game_color = GRAY  # Highlighted color
                else:
                    load_game_color = BLACK  # Default color

            if event.type == pygame.MOUSEBUTTONDOWN:
                if new_game_rect.collidepoint(event.pos):
                    return "new_game"  # Return "new_game" when New Game is selected
                elif load_game_rect.collidepoint(event.pos):
                    return "load_game"  # Return "load_game" when Load Game is selected

        screen.fill(WHITE)
        draw_text(new_game_text, menu_font, new_game_color, screen, new_game_rect.x, new_game_rect.y)
        draw_text(load_game_text, menu_font, load_game_color, screen, load_game_rect.x, load_game_rect.y)

        pygame.display.flip()

# Main game loop
def main():
    while True:
        choice = main_menu()
        if choice == "new_game":
            selected_class, selected_race, selected_alignment, character_name, stats = choose_character()
            print("Character Class:", selected_class)
            print("Character Race:", selected_race)
            print("Character Name:", character_name)
            print("Character Stats:", stats)  # Print stats
        elif choice == "load_game":
            print("Load Game selected")

if __name__ == "__main__":
    main()