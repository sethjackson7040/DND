import openai

# Set your API key here
api_key = "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"

# Initialize the OpenAI API client
openai.api_key = api_key

# Example prompt
prompt = "Once upon a time in a faraway land..."

# Generate text
response = openai.Completion.create(
    engine="text-davinci-002",  # You can choose the appropriate engine
    prompt=prompt,
    max_tokens=50,  # Adjust as needed
)

# Print the generated text
print(response.choices[0].text)