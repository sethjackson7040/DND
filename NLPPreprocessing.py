import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import string

# Download NLTK resources (if not already downloaded)
nltk.download('punkt')
nltk.download('stopwords')

# Define the input and output file paths
input_file = 'fantasy_corpus.txt'
output_file = 'cleaned_fantasy_corpus.txt'

# Read the content of the input file
with open(input_file, 'r', encoding='utf-8') as file:
    text = file.read()

print("Read input file successfully.")

# Tokenization
tokens = word_tokenize(text)

print("Tokenization complete.")

# Remove punctuation and convert to lowercase
cleaned_tokens = [token.lower() for token in tokens if token.isalnum()]

print("Punctuation removal and lowercase conversion complete.")

# Remove stop words
stop_words = set(stopwords.words('english'))
filtered_tokens = [token for token in cleaned_tokens if token not in stop_words]

print("Stop words removal complete.")

# Join the filtered tokens into a single string
cleaned_text = ' '.join(filtered_tokens)

# Save the cleaned text to the output file
with open(output_file, 'w', encoding='utf-8') as file:
    file.write(cleaned_text)

print("Data preprocessing and cleaning complete. Cleaned text saved to", output_file)
